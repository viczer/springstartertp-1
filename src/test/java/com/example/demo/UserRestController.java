package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import com.example.demo.dao.UserRepository;

import com.example.demo.models.Users;
@RunWith(SpringRunner.class)
@DataJpaTest
class UserRestController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetUsers() {
		Users p1 = new Users("admin", "admin", "11/06/1971", "Republic", "Nice", "06100", "0777777777");
		Users p2 = new Users("Vik", "Vik", "11/06/1981", "Republic", "Nice", "06100", "0666666666");
		entityManager.persist(p1);
		entityManager.persist(p2);
		List<Users> AllUsersFromDb = userRepository.findAll();

		List<Users> usersList = new ArrayList<Users>();

		for (Users personne : AllUsersFromDb) {
			usersList.add(personne);
		}
		assertThat(usersList.size()).isEqualTo(2);

	}

	@Test
	void testGetUserById() {
		Users p1 = new Users("admin", "admin", "11/06/1971", "Republic", "Nice", "06100", "0777777777");
		Users usersSavedInDb = entityManager.persist(p1);
		Users usersFromDb = userRepository.getOne(usersSavedInDb.getId());
		assertEquals(usersSavedInDb, usersFromDb);
		assertThat(usersFromDb.equals(usersSavedInDb));
	}

	@Test
	void testUpdateUser() {
		Users p1 = new Users("admin", "admin", "11/06/1971", "Republic", "Nice", "06100", "0777777777");
		entityManager.persist(p1);
		Users getFromDb = userRepository.getOne(p1.getId());
		getFromDb.setNom("admino");
		entityManager.persist(getFromDb);

		List<Users> AllUsersFromDb = userRepository.findAll();
		assertThat(AllUsersFromDb.contains(getFromDb));
	}

	@Test
	void testDeleteUser() {
		Users p1 = new Users("admin", "admin", "11/06/1971", "Republic", "Nice", "06100", "0777777777");
		Users p2 = new Users("Vik", "Vik", "11/06/1981", "Republic", "Nice", "06100", "0666666666");
		Users persist = entityManager.persist(p1);
		entityManager.persist(p2);

		entityManager.remove(persist);
		List<Users> AllUsersFromDb = userRepository.findAll();
		List<Users> userList = new ArrayList<>();

		for (Users users : AllUsersFromDb) {
			userList.add(users);
		}
		assertThat(userList.size()).isEqualTo(1);
	}

	@Test
	void testSave() {
		Users p1 = new Users("admin", "admin", "11/06/1971", "Republic", "Nice", "06100", "0777777777");
		Users usersSavedInDb = entityManager.persist(p1);
		Users usersFromDb = userRepository.getOne(usersSavedInDb.getId());
		assertEquals(usersSavedInDb, usersFromDb);
		assertThat(usersFromDb.equals(usersSavedInDb));
	}

}
