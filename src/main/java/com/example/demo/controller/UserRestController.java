package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.UserRepository;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.models.Users;

@RestController
public class UserRestController {
	@Autowired
	private UserRepository userRepository;

	@GetMapping(value = "/users")
	public List<Users> getUsers() {
		return userRepository.findAll();
	}

//	@RequestMapping(value = "/personnes", method = RequestMethod.GET)
//	public List<User> getUsers() {
//		return personneRepository.findAll();
//	}

	@GetMapping(value = "/users/{id}")
	public ResponseEntity<Users> getUserById(@PathVariable Long id) {
		Users user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		return ResponseEntity.ok().body(user);
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<Users> updateUser(@RequestBody Users p, @PathVariable(value = "id") Long id)
			throws ResourceNotFoundException {
		Users user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		user.setNom(p.getNom());
		user.setPrenom(p.getPrenom());
		user.setDateAnniversaire(p.getDateAnniversaire());
		user.setRue(p.getRue());
		user.setVille(p.getVille());
		user.setCodePostal(p.getCodePostal());
		user.setTel(p.getTel());
		final Users updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}
//	@RequestMapping(value = "/personnes/{id}", method = RequestMethod.GET)
//	public User getUserById(@PathVariable Long id) {
//		return personneRepository.findById(id).get();
//	}

	@DeleteMapping("/users/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		Users user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	@PostMapping(value = "/users")
	public Users save(@RequestBody Users p) {
		return userRepository.save(p);
	}


}