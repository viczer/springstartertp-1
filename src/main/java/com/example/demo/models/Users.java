package com.example.demo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String prenom;
	private String dateAnniversaire;
	private String rue;
	private String ville;
	private String codePostal;
	private String tel;

	public Users() {
	}

	public Users(String nom, String prenom, String dateAnniversaire, String rue, String ville, String codePostal,
			String tel) {

		this.nom = nom;
		this.prenom = prenom;
		this.dateAnniversaire = dateAnniversaire;
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
		this.tel = tel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateAnniversaire() {
		return dateAnniversaire;
	}

	public void setDateAnniversaire(String dateAnniversaire) {
		this.dateAnniversaire = dateAnniversaire;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateAnniversaire=" + dateAnniversaire
				+ ", rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + ", tel=" + tel + "]";
	}

}
